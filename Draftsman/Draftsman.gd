tool
extends Node
# To use draw in debug mode add 'tool' keyword at the top of your node's script.
# To use draw ONLY in debug mode add 'if Engine.is_editor_hint():' before
# Draftsman.draw_... calls.
# To change the font simply edit the DraftsmanFont.tres.


const DASH_LENGTH: float = 10.0
const LINE_WIDTH: float = 2.0
const ANTIALIASED: bool = false
const COLOR: Color = Color.lightblue
const ANIMATE: bool = true

var FONT:DynamicFont setget set_font, get_font


func set_font(value):
	FONT = value


func get_font():
	if FONT == null:
		FONT = load("res://Draftsman/DraftsmanFont.tres") as DynamicFont
	return FONT


func get_time_multiplier():
	return OS.get_system_time_msecs() / 300.0


func draw_circle(drawFrom:  CanvasItem, radius: float, 
		offset: Vector2 = Vector2.ZERO, width: float = LINE_WIDTH, 
		antialiased: bool = ANTIALIASED):
	drawFrom.draw_arc(offset, radius, 0, TAU, 36, 
		COLOR, width, antialiased)


func draw_circle_dashed(drawFrom: CanvasItem, radius: float,
		offset: Vector2 = Vector2.ZERO, width: float = LINE_WIDTH,
		antialiased: bool = ANTIALIASED):
	var dashAngle = deg2rad(DASH_LENGTH)
	var moveBy = deg2rad(fmod(get_time_multiplier(), 360))
	for i in range(0, 360, DASH_LENGTH):
		var from = i * dashAngle + (moveBy if ANIMATE else 0)
		var to = from + dashAngle 
		drawFrom.draw_arc(offset, radius, from, to, 10, 
				COLOR, width, antialiased)


func draw_line(drawFrom: CanvasItem, pStart: Vector2, pEnd: Vector2, 
		lineColor: Color = COLOR, lineWidth: float = LINE_WIDTH, 
		antialiased: bool = ANTIALIASED):
	drawFrom.draw_line(pStart, pEnd, lineColor, lineWidth, antialiased)


func draw_line_dashed(drawFrom: CanvasItem, pStart: Vector2, pEnd: Vector2, 
		lineColor: Color = COLOR, lineWidth: float = LINE_WIDTH, 
		antialiased: bool = ANTIALIASED):
	var maxLines = (pEnd - pStart).length() / DASH_LENGTH
	var direction = (pEnd - pStart).normalized()
	var timeOffset = direction * fmod(get_time_multiplier(), maxLines)
	for i in range(0, maxLines, 2):
		var dashStart = (pStart + direction * i * DASH_LENGTH + 
				(timeOffset if ANIMATE else 0))
		var dashEnd = dashStart + direction * DASH_LENGTH
		drawFrom.draw_line(dashStart, dashEnd, 
				lineColor, lineWidth, antialiased)


func draw_string(drawFrom: CanvasItem, text: String, centered: bool = true, 
		offset: Vector2 = Vector2.ZERO, font: Font = null):
	if font == null:
		font = get_font()
	if centered:
		offset -= font.get_string_size(text) / 2
	drawFrom.draw_string(font, offset, text)


func draw_multiline(drawFrom: CanvasItem, points: PoolVector2Array, 
		width: float = LINE_WIDTH, color: Color = COLOR, 
		antialiased: bool = ANTIALIASED):
	for index in points.size() - 2:
		draw_line(drawFrom,points[index], points[index+1],
				color, width, antialiased)


func draw_force(drawFrom: CanvasItem, gravScale: float, points: int,
		time: float, angleRad: float, force: float, 
		width: float = LINE_WIDTH, color: Color = COLOR,
		antialiased: bool = ANTIALIASED):
	var pointsArray = PoolVector2Array([])
	var grav = gravScale * 14
	var x = force * cos(angleRad)
	var y = force * sin(angleRad)
	for point in points:
		var localtime = time * point / points
		var newPoint = Vector2(localtime * x, 
				localtime * y + grav * localtime * localtime)
		pointsArray.append(newPoint)
	draw_multiline(drawFrom, pointsArray, width, color, antialiased)
